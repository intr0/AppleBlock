[![pipeline status](https://gitlab.com/intr0/AppleBlock/badges/master/pipeline.svg)](https://gitlab.com/intr0/AppleBlock/commits/master)

[![forthebadge](https://forthebadge.com/images/badges/fuck-it-ship-it.svg)](https://forthebadge.com)

***

### Please see [apple-telemetry](https://github.com/adversarialtools/apple-telemetry) for a maintained project by [@adversarialtools](https://github.com/adversarialtools) over at GitHub.

### [AppleBlock.txt](https://gitlab.com/intr0/AppleBlock/raw/master/AppleBlock.txt) is a simple sub/domain based blacklist to block all outgoing and incoming connections to/from iOS and Apple.

```
https://gitlab.com/intr0/AppleBlock/raw/master/AppleBlock.txt
```

***

### IMPORTANT:

- **This list is ongoing; Apple does worm its way into iOS so it can have its peek at everything, change and even kill both settings and applications.**
- **Redundancies exist; this is a built-in feature.**
- **Please never think this list will be anything but ongoing. And please help out by adding to this blacklist. Thank-you.**

***

[AntiRevoke.hosts](https://gitlab.com/intr0/AppleBlock/raw/master/AntiRevoke.hosts)

```
https://gitlab.com/intr0/AppleBlock/raw/master/AntiRevoke.hosts
```

**AntiRevoke.hosts is designed to block iOS updates & revocations. It is a tiny hosts file & does not block all iOS<-->Apple connections. It's important to note that while AppleBlock.txt has been fully tested and is 100% functional, AntiRevoke.hosts is experimemtal and has not been fully tested against Beta App builds being removed from iDevices.**

***

- **I recommend using AppleBlock.txt instead of AntiRevoke.hosts so that you will be able to block (most) communications between iOS devices and Apple.**
- **Using AppleBlock.txt serves the dual purpose of acting as the AntiRevoke.hosts file and of denying Apple its data mining operations disguised as necessary in/outgoing OTA connections with iOS devices.**
- **It's important to know that even those connections most iOS users think of as important services and that Apple forces users to have to use in order to connect to, e.g., its App store & iCloud, does not make them harmless.**
- **Using iCloud exposes any personal information Apple has collected from you for literally all to see. iCloud mail stores everything you send and receive in PLAIN TEXT.**
- **Don't fall for Apple's campaign to make (potential)users believe iOS is a private, secure, fully encrypted platform. Doing so will put your privacy at severe risk.**

***